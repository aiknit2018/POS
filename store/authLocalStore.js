export const state = () =>({
    userData: null
})

export const mutations = {
    Login(state, payload){
    state.userData = payload
    },
    Logout(state){
        state.userData = null
    },
}