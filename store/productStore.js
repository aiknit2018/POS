export const state = () =>({
    proData: [],
})

export const mutations = {
    addProductToproData(state, payload){
        state.proData.push(payload)
    },
    deleteProduct(state, payload){
        state.proData.splice(payload,1)
    },
    updateProduct(state, payload){
        state.proData = state.proData.map((each) => each.productId ===  payload.productId ?  payload : each)
    },
    updateAmountProduct(state, payload){
        //  { ...each, productQty: each.productQty - aount }
     //console.log(state.proData.map((each, index) => each.productId ===  payload.productId ?  {...each, productQty : parseInt(each.productQty - payload.amount)} : each))
        state.proData =  state.proData.map((each) => each.productId ===  payload.productId ?  {...each, productQty : parseInt(each.productQty - payload.amount)} : each)
    },
    updateAmountcanceltempOrder(state, payload){
        // console.log(state.proData.map((each, index) => each.productId ===  payload.productId ?  {...each, productQty : parseInt(each.productQty) + parseInt(payload.amount)  } : each))
        state.proData =  state.proData.map((each) => each.productId ===  payload.productId ?  {...each, productQty : parseInt(each.productQty) + parseInt(payload.amount)  } : each)
    },
}