export const state = () =>({
    tempOrderData: []
})

export const mutations = {
    addOrdersTotempOrderData(state, payload){
    state.tempOrderData.push(payload)
    
    },
    canceltempOrderData(state, payload){
        state.tempOrderData.splice(payload,1)
    },
    updatetempOrderData(state, payload){
        state.tempOrderData = state.tempOrderData.map((each) => each.orderId ===  payload.orderId ?  payload : each)
    },
    removeTempOrderData(state, payload){
        console.log("log payload", payload)
        const newArray = state.tempOrderData.filter((each, index) => {
            if(!payload.includes(index)){
                return each
            }
        })
        console.log("new array", newArray)

        state.tempOrderData = newArray
    }
}