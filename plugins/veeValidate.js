import Vue from 'vue'
import VeeValidate , { Validator } from 'vee-validate'

const dictionary = {
 en: {
   custom: {
    tableName:{
      required: 'กรุณากรอกข้อมูลโต๊ะ',
    },
    productName: {
       required: 'กรุณากรอกชื่อสินค้า',
     },
     productNameSelect:{
      required: 'กรุณาเลือกสินค้า',
     },
     productCategory: {
        required: 'กรุณาเลือกประเภทสินค้า',
      },
      productCapitalPrice: {
        required: 'กรุณากรอกราคาทุน',
        numeric: 'กรุณาใส่เฉพาะตัวเลขเท่านั้น',
        min_value: 'กรุณาใส่ราคาต้นทุนตั้งแต่ 1 ขึ้นไป'
      },
      productSalePrice: {
        required: 'กรุณากรอกราคาทุน',
        numeric: 'กรุณาใส่เฉพาะตัวเลขเท่านั้น',
        min_value: 'กรุณาใส่ราคาตั้งแต่ 1 ขึ้นไป'
      },
      productQty: {
        required: 'กรุณากรอกจำนวน',
        numeric: 'กรุณาใส่เฉพาะตัวเลขเท่านั้น',
        min_value: 'กรุณาใส่จำนวนตั้งแต่ 1 ขึ้นไป'
      },
      amount: {
        required: 'กรุณากรอกจำนวน',
        numeric: 'กรุณาใส่เฉพาะตัวเลขเท่านั้น',
        min_value: 'กรุณาใส่จำนวนตั้งแต่ 1 ขึ้นไป'
      },
      productMinimum: {
        required: 'กรุณากรอกจำนวนสินค้าขั้นต่ำ',
        numeric: 'กรุณาใส่เฉพาะตัวเลขเท่านั้น',
        min_value: 'กรุณาใส่จำนวนขั้นต่ำตั้งแต่ 1 ขึ้นไป'
      },
      username: {
        required: 'กรุณากรอกชื่อผู้ใช้งาน',
      },
      password: {
        required: 'กรุณากรอกรหัสผ่าน',
      }
   }
 }
};

Vue.use(VeeValidate)
Validator.localize(dictionary)